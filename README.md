# Steps to follow to run project

## clone the project

    $ git clone https://gitlab.com/Amin2a/smart_test_tirage.git


## install dependencies

    $ composer install


## create the database

 `$ php bin/console doctrine:schema:create:database`

 
 

## update schema with doctrine migrations bundle
`php bin/console doctrine bin/console doctrine:migrations:migrate
`

## export SQL sous smart_test_tirage
tirage.sql
