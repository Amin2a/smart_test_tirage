<?php


namespace App\DBAL;

/**
 * Class EnumTirageType
 */
class EnumTirageType extends EnumType
{
    const TIRAGE_SPECIAL = 'special';
    const TIRAGE_NUMBER = 'number';
    protected $name = 'enumtirage';
    protected $values = [self::TIRAGE_SPECIAL, self::TIRAGE_NUMBER];
}