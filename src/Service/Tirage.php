<?php


namespace App\Service;


use App\Entity\Question;
use App\Entity\Result;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use DateTime;

class Tirage
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @param DateTime $date
     * @return iterable<Result>
     */
    public function getTirageResult(DateTime $date): iterable
    {
        $this->logger->info("Tirage pour la date : {$date->format('Y-m-d')}");
        $result = $this->em->getRepository(Result::class)->getResultByDate($date);

        $tirages = [];
        foreach ($result->getTirages() as $tirage) {
            $tirages[] = [
                'id' => $tirage->getId(),
                'value' => $tirage->getValue(),
                'type' => $tirage->getType(),
                'drawIndex' => $tirage->getDrawIndex(),
            ];
        }

        $data = [
            'eid' => $result->getId(),
            'date' => $result->getDate()->format('Y-m-d'),
            'resultat' => $tirages
        ];

        return $data;
    }
}
