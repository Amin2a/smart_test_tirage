<?php

namespace App\Controller;

use App\Service\Tirage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ResultController
 */
class ResultController extends AbstractController
{
    /**
     * retrieve Result Tirage By date
     *
     * @Route("/result/{date}", name="result_tirage", methods={"GET"})
     * @param Request  $request
     * @param Tirage   $tirage
     *
     * @return JsonResponse
     */
    public function index(Request $request, Tirage $tirage, string $date):JsonResponse
    {
        if(!strtotime($date)) {
            return new JsonResponse("invalid date {$date}",Response::HTTP_BAD_REQUEST);
        }
        $data = $tirage->getTirageResult(new \DateTime($date));

       if(empty($data)) {
           return new JsonResponse("aucun tirage trouvé pour la date de {$date}",Response::HTTP_OK);

       }
        return new JsonResponse($data, Response::HTTP_OK);
    }
}
