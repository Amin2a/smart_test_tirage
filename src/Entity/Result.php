<?php


namespace App\Entity;


use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Result
 * @ORM\Entity(repositoryClass="App\Repository\ResultRepository")
 */
class Result
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Tirage", mappedBy="result", cascade={"persist", "remove"})
     */
    private $tirages;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @return Collection
     */
    public function getTirages(): Collection
    {
        return $this->tirages;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @param Collection $tirages
     */
    public function setTirages(Collection $tirages): void
    {
        $this->tirages = $tirages;
    }
}