<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tirage
 * @ORM\Entity()
 */
class Tirage
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(type="enumtirage")
     */
    private $type;

    /**
     * @var string
     * @ORM\Column (type="string")
     */
    private $value;

    /**
     * @var  integer
     * * @ORM\Column (type= "integer")
     */
    private $drawIndex;

    /**
     * @var Result
     *
     * @ORM\ManyToOne(targetEntity="Result",  inversedBy="tirages")
     */
    private $result;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getDrawIndex(): int
    {
        return $this->drawIndex;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @param int $drawIndex
     */
    public function setDrawIndex(int $drawIndex): void
    {
        $this->drawIndex = $drawIndex;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }


}