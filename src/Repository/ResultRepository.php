<?php


namespace App\Repository;


use App\Entity\Result;
use Doctrine\ORM\EntityRepository;
use DateTime;

/**
 * Class ResultRepository
 */
class ResultRepository extends EntityRepository
{
    /**
     * @param DateTime $date
     * @return Result
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getResultByDate(DateTime $date): Result
    {
        $qb = $this->createQueryBuilder('res')
            ->where('res.date = :date')
            ->setParameter('date', $date->format('Y-m-d'));
        return $qb->getQuery()->getOneOrNullResult();
    }


}