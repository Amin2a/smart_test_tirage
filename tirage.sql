-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 16 juil. 2021 à 11:47
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `smart_test`
--

-- --------------------------------------------------------

--
-- Structure de la table `tirage`
--

DROP TABLE IF EXISTS `tirage`;
CREATE TABLE IF NOT EXISTS `tirage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `result_id` int(11) DEFAULT NULL,
  `type` enum('special','number') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:enumtirage)',
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `draw_index` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2A145AFF7A7B643` (`result_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tirage`
--

INSERT INTO `tirage` (`id`, `result_id`, `type`, `value`, `draw_index`) VALUES
(3, 1, 'special', '11', 1),
(4, 1, 'special', '13', 1),
(5, 1, 'number', '654', 1),
(6, 1, 'number', '98', 1),
(7, 2, 'special', '123', 1),
(8, 2, 'number', '76', 1),
(9, 2, 'special', '123', 1),
(10, 2, 'number', '76', 1),
(11, 2, 'number', '45', 1),
(12, 3, 'number', '98', 1),
(13, 3, 'special', '123', 1),
(14, 3, 'number', '76', 1),
(15, 3, 'special', '123', 1),
(16, 3, 'number', '76', 1),
(17, 3, 'number', '45', 1),
(18, 4, 'number', '98', 1),
(19, 4, 'special', '123', 1),
(20, 4, 'number', '76', 1),
(21, 5, 'special', '123', 1),
(22, 5, 'number', '76', 1),
(23, 5, 'number', '45', 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `tirage`
--
ALTER TABLE `tirage`
  ADD CONSTRAINT `FK_2A145AFF7A7B643` FOREIGN KEY (`result_id`) REFERENCES `result` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
