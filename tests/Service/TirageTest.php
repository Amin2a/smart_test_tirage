<?php


namespace App\Tests\Service;

use App\Entity\Result;
use App\Repository\ResultRepository;
use App\Service\Tirage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Psr\Log\Test\TestLogger;

class TirageTest extends TestCase
{
    /**
     * @covers \App\Service\Tirage::getTirageResult
     */
    public function testIndex()
    {
        $tirage1 = $this->getTirageMock('number');
        $tirage2 = $this->getTirageMock('number');
        $tirage3 = $this->getTirageMock('special');
        $tirage4 = $this->getTirageMock('number');
        $tirage5 = $this->getTirageMock('special');
        $result = $this->createMock(Result::class);
        $result->expects($this->any())->method('getId')->willReturn(123);
        $result->expects($this->any())->method('getDate')->willReturn(new \DateTime('2021-07-15'));
        $result->expects($this->any())->method('getTirages')
            ->willReturn(new ArrayCollection([$tirage1, $tirage2, $tirage3, $tirage4, $tirage5]));


        $entityManagerMock  = $this->createMock(EntityManagerInterface::class);
        $repository = $this->createMock(ResultRepository::class);
        $repository->expects($this->any())
            ->method('getResultByDate')
            ->willReturn($result);
        $entityManagerMock->expects($this->any())->method('getRepository')->willReturn($repository);

        $tirageService = new Tirage($entityManagerMock, new TestLogger());
        $datas = $tirageService->getTirageResult(new \DateTime('2021-07-15'));

        self::assertCount(5, $datas['resultat']);
        self::assertSame(123, $datas['eid']);
        self::assertSame( '2021-07-15', $datas['date']);
    }

    private function getTirageMock(string $type)
    {
        $tirage = $this->createMock(\App\Entity\Tirage::class);
        $tirage->expects($this->any())
            ->method('getId')
            ->willReturn(334);
        $tirage->expects($this->any())
            ->method('getValue')
            ->willReturn('value');
        $tirage->expects($this->any())
            ->method('getType')
            ->willReturn($type);

        return $tirage;

    }

}